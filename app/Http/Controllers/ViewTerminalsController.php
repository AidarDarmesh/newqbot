<?php

namespace App\Http\Controllers;

use App\Terminal;
use Illuminate\Http\Request;

class ViewTerminalsController extends Controller
{
    /**
     * Вью всех терминалов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index()
	{
		$terminals = Terminal::all();

		return view('terminals.index', [
										'terminals' => $terminals
										]);
	}

    /**
     * Вью для добавления терминала
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function addGet()
	{
		return view('terminals.add');
	}

    /**
     * Пост обработка добавления терминала
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
	public function addPost(Request $request)
	{
		$info = $request->all();

        do {
            $id = rand(1000, 9999);
            $terminal = Terminal::find($id);
        } while ($terminal !== null);

        $terminal        = new Terminal();
        $terminal->id    = $id;
        $terminal->phone = $info['phone'];
        $terminal->inet  = $info['inet'];
        $terminal->pages = $info['pages'];
        $terminal->toner = $info['toner'];
        $terminal->addr  = $info['addr'];

		$terminal->save();

		return redirect('terminals');
	}

    /**
     * Вью деталей терминала
     *
     * @param Terminal $terminal
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function details(Terminal $terminal)
	{
		return view('terminals.details', [
								'terminal' => $terminal
								]);
	}

    /**
     * Вью редактирования клиента
     *
     * @param Terminal $terminal
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function editGet(Terminal $terminal)
	{
		return view('terminals.edit', [
							'terminal' => $terminal
							]);
	}

    /**
     * Пост обработка редактирования терминала
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function editPost(Request $request)
	{
        $info              = $request->all();
        $terminal          = Terminal::find($info['id']);
        $terminal->phone   = $info['phone'];
        $terminal->traffic = $info['traffic'];
        $terminal->pages   = $info['pages'];
        $terminal->toner   = $info['toner'];
        $terminal->done    = $info['done'];
        $terminal->addr    = $info['addr'];

		$terminal->save();

		return redirect('terminals/' . $terminal->id);
	}

    /**
     * Удаление терминала
     *
     * @param Terminal $terminal
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function delete(Terminal $terminal)
	{
		$terminal->delete();

		return redirect('terminals');
	}
}
