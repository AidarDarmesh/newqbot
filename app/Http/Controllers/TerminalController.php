<?php

namespace App\Http\Controllers;

use App\Client;
use App\Order;
use App\Terminal;
use Illuminate\Http\Request;

class TerminalController extends Controller
{
    /**
     * Возврат терминалу данных о заказе и его клиенте
     *
     * @param $id
     *
     * @return null
     */
	public function order($order_id, $term_id)
	{
		$order = Order::find($order_id);

		if ($order !== null) {
            $order->client = $order->client;
            $order->terminal = $order->terminal;

            return $order;
        } else {
			return null;
		}
	}

	public function code($order_id, $term_id)
    {
        $order = Order::find($order_id);

        if ($order !== null) {
            if ($order->terminal->id == $term_id) {
                $code        = rand(1000, 9999);
                $order->code = $code;

                if ($order->save()) {
                    $order->client->send(
                        sprintf(
                            "СЕКРЕТНЫЙ КОД %u\nдля заказа %u",
                            $code,
                            $order->id
                        )
                    );
                }
            }

            return $order;
        } else {
            return null;
        }
    }

    /**
     * Если заказ распечатан
     *
     * @param       $terminalId
     * @param Order $order
     */
	public function printed($terminalId, Order $order)
	{
        $order->terminal_id = $terminalId;
        $order->printed_at  = date('Y-m-d H:i:s');

		$order->save();

        $terminal           = Terminal::find($terminalId);
        $terminal->pages   -= $order->pages * $order->copy;
        $terminal->inet    -= ($order->size + 0.5);
        $terminal->toner   -= $order->pages * $order->copy;
        $terminal->total   += $order->pages * $order->copy;

		$terminal->save();
		$order->client->send("Заказ " . $order->filename . " распечатан👍");
		$order->utilize();
	}

    /**
     * Возвращает данные о терминале
     *
     * @param Terminal $terminal
     *
     * @return Terminal
     */
	public function info(Terminal $terminal)
	{
		return $terminal;
	}
}
