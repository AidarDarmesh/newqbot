<?php

namespace App\Http\Controllers;

use App\Client;
use App\Order;
use Illuminate\Http\Request;

class ViewOrdersController extends Controller
{
    /**
     * Вывод всех инвойсов данного заказа
     *
     * @param Order $order
     *
     * @return array
     */
	public function invoices(Order $order)
	{
		return $order->invoices;
	}

    /**
     * Вью всех заказов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
    	return view('orders.index', [
    								'orders' => Order::all()
    								]);
    }

    /**
     * Вью для добавления заказа
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addGet()
    {
    	return view('orders.add');
    }

    /**
     * Пост обработка добавления заказа
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function addPost(Request $request)
    {
    	$info = $request->all();

        do {
            $id = rand(100000, 999999);
            $order = Order::find($id);
        } while ($order !== null);

        $order            = new Order();
        $order->id        = $id;
        $order->client_id = $info['client_id'];
        $order->filename  = $info['filename'];
        $order->url       = $info['url'];
        $order->pages     = $info['pages'];
        $order->size      = $info['size'];

		$order->save();

		return redirect('orders');
    }

    /**
     * Вью деталей заказа
     *
     * @param Order $order
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details(Order $order)
    {
    	return view('orders.details', [
    								'order' => $order,
    								'client' => $order->client
    								]);
    }

    /**
     * Вью редактирования заказа
     *
     * @param Order $order
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editGet(Order $order)
	{
		return view('orders.edit', [
							'order' => $order
							]);
	}

    /**
     * Пост обработка редактирования заказа
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function editPost(Request $request)
	{
        $info               = $request->all();
        $order              = Order::find($info['id']);
        $order->client_id   = $info['client_id'];
        $order->filename    = $info['filename'];
        $order->url         = $info['url'];
        $order->pages       = $info['pages'];
        $order->size        = $info['size'];
        $order->code        = $info['code'];
        $order->terminal_id = $info['terminal_id'];
        $order->printed_at  = $info['printed_at'];

		$order->save();

		return redirect('orders/' . $order->id);
	}

    /**
     * Удаление заказа
     *
     * @param Order $order
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Order $order)
	{
		$order->utilize();

		return redirect('orders');
	}
}
