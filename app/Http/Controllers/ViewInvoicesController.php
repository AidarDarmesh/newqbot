<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Order;
use Illuminate\Http\Request;

class ViewInvoicesController extends Controller
{
    /**
     * Вью всех инвойсов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index()
    {
    	return view('invoices.index', [
    								'invoices' => Invoice::all()
    								]);
    }

    /**
     * Вью деталей инвойса
     *
     * @param Client $client
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details(Invoice $invoice)
    {
    	return view('invoices.details', [
    								'invoice' => $invoice
    								]);
    }

    /**
     * Удаление инвойса
     *
     * @param Invoice $invoice
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Invoice $invoice)
	{
		$invoice->delete();

		return redirect('invoices');
	}
}
