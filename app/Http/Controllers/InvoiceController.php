<?php

namespace App\Http\Controllers;

use App\Button;
use App\Client;
use App\Invoice;
use App\Keyboard;
use App\Order;
use App\Terminal;
use Illuminate\Http\Request;

require_once('paysys/kkb.utils.php');

class InvoiceController extends Controller
{
    /**
     * Банковская комиссия
     */
    const COMMISSION = 20;

    /**
     * Цена за страницу
     */
    const PRICE_PER_PAGE = 10;

    public function pay(Order $order) {
        if ($order === null) {
            return "Такого заказа нет";
        } else {
            if ($order->printed_at !== null) {
                return "Заказ уже был распечатан";
            } else {
                if ($order->paid_at !== null) {
                    return "Заказ уже оплачен";
                } else {
                    $price             = $order->pages * self::PRICE_PER_PAGE + self::COMMISSION;
                    $invoice           = new Invoice();
                    $invoice->order_id = $order->id;
                    $invoice->price    = $price;

                    $invoice->save();

                    return view('pay', [
                        'price'      => $price,
                        'invoice_id' => $invoice->id,
                    ]);
                }
            }
        }
    }

    public function paypost(Request $request) {
        $data = $request->all();

        if ($data['card1'] === '1234' && $data['card2'] === '1234'
            && $data['card3'] === '1234' && $data['card4'] === '1234') {
            if ($data['name'] === 'Vasya Pupkin') {
                if ($data['month'] === '07' && $data['year'] === '22') {
                    if ($data['cvv'] === '740') {
                        $invoice                = Invoice::find($data['invoice_id']);
                        $invoice->confirmed     = date('Y-m-d H:i:s');
                        $invoice->error         = 1234;
                        $invoice->card          = 1234;
                        $invoice->amount        = 1234;
                        $invoice->currency      = 1234;
                        $invoice->response_code = 1234;
                        $invoice->approval_code = 1234;
                        $invoice->reference     = 1234;
                        $invoice->approved      = date('Y-m-d H:i:s');
                        $invoice->approve_error = 1234;

                        $invoice->save();

                        $order          = $invoice->order;
                        $order->paid_at = date('Y-m-d H:i:s');
                        $client         = $order->client;

                        $order->save();

                        $client->send(
                            sprintf(
                                "Заказ %s оплачен👏\n" .
                                "Срок действия 3 часа\n" .
                                "Введи %d в терминал 👉\n" .
                                "%s\n" .
                                "ВОЗЬМИ СМАРТФОН С СОБОЙ\n" .
                                "Я ПРИШЛЮ КОД",
                                $order->filename,
                                $order->id,
                                $order->terminal->addr
                            )
                        );

                        $client->sendKeyboard(
                            Keyboard::invoice(),
                            "Не тяни с распечаткой!"
                        );

                        return view('return', ['link' => 'https://vk.me/newqbot']);
                    } else {
                        return 'Введите CVV/CVC правильно';
                    }
                } else {
                    return 'Введите срок действия правильно';
                }
            } else {
                return 'Введите имя правильно';
            }
        } else {
            return 'Введите номер карты правильно';
        }
    }
    
	protected $config_path = "paysys/config.txt";

	public function invoice(Order $order)
	{
		if ($order === null)
		{
			return "Такого заказа нет";
		} else {
			if ($order->terminal_id !== null)
			{
				return "Заказ уже был распечатан";
			} else {
				if ($order->code !== null)
				{
					return "Заказ уже оплачен";
				} else {
					
					// Данные для оплаты
					$currency_id = "398";
					$config_path = "paysys/config.txt";
					$price = $order->pages * 10 + 20;

					// Создание инвойса
					$invoice = new Invoice();
					$invoice->order_id = $order->id;
					$invoice->price = $price;
					$invoice->save();

					$url = 'https://epay.kkb.kz/jsp/process/logon.jsp';
					$email = 'qagazbot@gmail.com';
					$lang = 'rus';
					$backlink = url('/backlink');
					$fail_backlink = url('/fail_backlink');
					$postlink = url('/postlink');
					$fail_postlink = url('/fail_postlink');

					// Подписываем заказ
					$signed_order = process_request($invoice->id, $currency_id, $price, $this->config_path);

					return view('invoice', [
											'url' => $url,
											'signed_order' => $signed_order,
											'email' => $email,
											'lang' => $lang,
											'backlink' => $backlink,
											'fail_backlink' => $fail_backlink,
											'postlink' => $postlink,
											'fail_postlink' => $fail_postlink,
											]);

				}
			
			}

		}

	}

	public function backlink()
	{

		return "<a href='https://vk.me/qagaz_bot'>Вернуться к боту</a>";

	}

	public function fail_backlink()
	{

		return "Вы не оплатили заказ";

	}

	public function postlink(Request $request)
	{

		// Получаю данные об авторизации средств клиента
		$response = $_POST['response'];

		// Расшифровка
		$result = process_response(stripslashes($response), $this->config_path);

		$error = $card = $amount = $currency = $response_code = $approval_code = $reference = null;

		$invoice_id = empty($result['ORDER_ORDER_ID']) ? null : (int)$result['ORDER_ORDER_ID'];

		if (is_array($result)) {

			if (in_array("ERROR", $result)) {

				$error = "TYPE: $result[ERROR_TYPE], CODE: $result[ERROR_CODE], DATA: $result[ERROR_CHARDATA], TIME: $result[ERROR_TIME]";

        		$time = $result['ERROR_TIME'];

			}

			if (in_array("DOCUMENT", $result)) {

	            $time = $result['RESULTS_TIMESTAMP'];

	            $card = $result['PAYMENT_CARD'];

	            $amount = $result['PAYMENT_AMOUNT'];

	            $currency = $result['ORDER_CURRENCY'];

	            $response_code = $result['PAYMENT_RESPONSE_CODE'];

	            $approval_code = $result['PAYMENT_APPROVAL_CODE'];

	            $reference = $result['PAYMENT_REFERENCE'];

	        }

		} else {

			$error = 'INVALID_RESULT_FORMAT';

		}

		if ($result['CHECKRESULT'] != '[SIGN_GOOD]') {

		        $error = $result['CHECKRESULT'];

		    }

	    // Обновляю БД
	    $invoice = Invoice::find($invoice_id);
	    $invoice->confirmed = date('Y-m-d H:i:s');
	    $invoice->error = $error;
	    $invoice->card = $card;
	    $invoice->amount = $amount;
	    $invoice->currency = $currency;
	    $invoice->response_code = $response_code;
	    $invoice->approval_code = $approval_code;
	    $invoice->reference = $reference;
	    $invoice->save();

		// Списываю средства
	    $xml = urlencode(process_complete($reference, $approval_code, $invoice_id, $currency, $amount, $this->config_path));

	    $response = file_get_contents("https://epay.kkb.kz/jsp/remote/control.jsp?$xml");

	    $result = process_response(stripslashes($response), $this->config_path);

	    $error = null;

	    $invoice_id = empty($result['PAYMENT_ORDERID']) ? null : $result['PAYMENT_ORDERID'];

	    $approved = false;

	    if (is_array($result)) {

	        if (in_array("ERROR", $result)) {

	            $error = "TYPE: $result[ERROR_TYPE], CODE: $result[ERROR_CODE], DATA: $result[ERROR_CHARDATA], TIME: $result[ERROR_TIME]";

	            $time = $result['ERROR_TIME'];

	        }
	        if (in_array("DOCUMENT", $result) && !empty($result['RESPONSE_MESSAGE'])
	            && strtolower($result['RESPONSE_MESSAGE']) == 'approved' && !empty($result['COMMAND_TYPE'])
	            && strtolower($result['COMMAND_TYPE']) == 'complete') {

	            $approved = true;

	        }
	    } else {

	        $error = 'INVALID_RESULT_FORMAT';

	    }

	    if ($result['CHECKRESULT'] != '[SIGN_GOOD]') {

	        $error = $result['CHECKRESULT'];

	    }

	    // Обновляю БД
	    $invoice->approved = date('Y-m-d H:i:s');
	    $invoice->approve_error = $error;
	    $invoice->save();

	    if($error === null)
	    {

			// Создаю код аутентификации и отправляю его клиенту
			$order = $invoice->order;
			$client = $order->client;
			$code = rand(1000, 9999);
			$order->code = $code;
			$order->save();
			$client->send(
							"Заказ " . $order->id . " оплачен👏\n" . 
							"Код аутентификации: " . $auth_code . " \n" . 
							"Введи номер заказа и код в терминал 👉\n" . 
							"МУИТ, 1 этаж, холл"
						);

			echo '0';

	    }

	}

	public function fail_postlink()
	{

		return  "Списание средств не удалось по следующим причинам:\n" . 
				"1) Разблокируйте карту\n" . 
				"2) Недостаточно средств";

	}

}
