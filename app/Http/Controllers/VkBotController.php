<?php

namespace App\Http\Controllers;

use App\Jobs\HandleVkMessage;
use Illuminate\Http\Request;

class VkBotController extends Controller
{
	public function messageNew(Request $request)
	{
		$data = json_decode($request->getContent());

		switch ($data->type) {
			case "confirmation":
				return config('vk.confkey');
			case "message_new":
        		if ($data->secret === config("vk.secret")) {
					dispatch(new HandleVkMessage($data->object));
				} else {
					echo "wrong secret";
				}
			return "ok";
		}
	}
}
