<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminal extends Model
{
    /**
     * Чтобы id не увеличивались при добавлении в БД
     * Id рандомное и неповторяющееся
     *
     * @var bool
     */
	public $incrementing = false;

	public function charge()
	{
		return $this->addr . " \nЗаряд: " . $this->pages . " стр\n";
	}

	public function about()
	{
		return $this->addr . " \nЛисты: " . $this->pages . " \nТрафик: " . $this->traffic . " \nТонер: " . $this->traffic . " \n";
	}

}
