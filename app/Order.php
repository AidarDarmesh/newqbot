<?php

namespace App;

use App\Button;
use App\Client;
use App\Invoice;
use App\Keyboard;
use App\Terminal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Order extends Model
{
    /**
     * Максимально одновременное кол-во заказов
     */
    const MAX_NUMBER = 4;

    /**
     * Чтобы id не увеличивались при добавлении в БД
     * Id рандомное и неповторяющееся
     *
     * @var bool
     */
	public $incrementing = false;

    /**
     * Отношение много заказов к одному клиенту
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function client()
	{
		return $this->belongsTo('App\Client');
	}

    public function terminal()
    {
        return $this->belongsTo('App\Terminal');
    }

    /**
     * Отношение один заказ ко многим инвойсам
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
	public function invoices()
	{
		return $this->hasMany('App\Invoice');
	}

	public function create($clientId, $fileName, $url, $size)
    {
        do {
            $randId = rand(100000, 999999);
            $order  = Order::find($randId);
        } while ($order !== null);

        $client   = Client::find($clientId);
        $hashName = $randId . ".pdf";

        if (copy($url, $hashName)) {
            // Выполняю скрипт и делаю парсинг на то, что выйдет в консоли
            // Никаких знаков в Windows и обязательный ./ в Linux
            exec(
                sprintf(
                    "./pdfinfo %s",
                    $hashName
                ),
                $output
            );

            $pages = 0;

            foreach ($output as $op) {
                // Вытаскиваем число
                if(preg_match("/Pages:\s*(\d+)/i", $op, $matches) === 1){
                    // Кол-во страниц
                    $pages = intval($matches[1]);
                    break;
                }
            }

            unlink($hashName);

            if ($pages !== 0) {
                $ableToPrint = false;

                foreach (Terminal::all() as $terminal) {
                    if ($terminal->pages >= $pages) {
                        $ableToPrint = true;
                    }
                }

                if ($ableToPrint) {
                    $this->id        = $randId;
                    $this->client_id = $clientId;
                    $this->filename  = $this->translit($fileName);
                    $this->url       = $url;
                    $this->pages     = $pages;
                    $this->side      = 1;
                    $this->copy      = 1;
                    $this->size      = $size / pow(1024, 2);

                    if ($this->save()) {
                        $client->sendKeyboard(
                            Keyboard::sides($this->id),
                            "Выбери тип распечатки\nНАЖИМАЙ КНОПКИ, НЕ ВВОДИ ТЕКСТ. Я НЕ ПОЙМУ"
                        );
                    } else {
                        $client->send("Ошибка при оформлении заказа");
                    }
                } else {
                    $client->send("Терминалы разряжены, попробуйте позже 🙏");
                }
            } else {
                $client->send("Нет доступа к файлу");
            }
        } else {
            $client->send("Нет доступа к файлу 😔");
        }
    }

    /**
     * Удалеям заказ и все инвойсы на него
     */
	public function utilize()
	{
	    try {
            foreach($this->invoices as $invoice)
            {
                $invoice->delete();
            }

            return $this->delete();
        } catch (\Exception $e) {
	        return $e->getMessage();
        }
	}

    public function translit($str)
    {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '',    'ы' => 'y',   'ъ' => '', ' ' => '_',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '',    'Ы' => 'Y',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        return strtr($str, $converter);
    }
}
