<?php

namespace App;

class Button
{
    public static function create($payload, $label, $color)
    {
        return [
            'color' => $color,
            'action' => [
                'type' => 'text',
                'payload' => $payload,
                'label' => $label,
            ],
        ];
    }

    public static function createOrders()
    {
        return self::create(
            json_encode(
                [
                    'command'  => 'orders',
                ]
            ),
            'Orders',
            'primary'
        );
    }

    public static function createNew()
    {
        return self::create(
            json_encode(
                [
                    'command'  => 'start',
                ]
            ),
            'New',
            'positive'
        );
    }

    public static function createCancel($orderId)
    {
        return self::create(
            json_encode(
                [
                    'command'  => 'cancel',
                    'order_id' => $orderId,
                ]
            ),
            'Cancel',
            'negative'
        );
    }
}
