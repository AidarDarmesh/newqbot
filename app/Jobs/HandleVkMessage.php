<?php

namespace App\Jobs;

use App\Button;
use App\Client;
use App\Keyboard;
use App\Order;
use App\Terminal;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class HandleVkMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Объект сообщения
     *
     * @var array
     */
    public $object;

    public function __construct($object)
    {
        $this->object = $object;
    }

    /**
     * Обработка контента сообщения
     */
    public function handle()
    {
        $client = Client::find($this->object->user_id);

        if ($client === null) {
            $client = new Client();
            $client->signUp($this->object->user_id);
        }

        $command = '';
        $payload = [];

        if (property_exists($this->object, 'payload')) {
            $payload = json_decode($this->object->payload);
            $command = $payload->command;
        }

        switch ($command) {
            case 'start':
                $client->send(
                    "Привет!\n" .
                    "Отправляй мне PDF\n" .
                    "Плати картой\n" .
                    "Распечатывай в терминале\n" .
                    "Всё просто 😉"
                );

                break;
            case 'side':
                $order       = Order::find($payload->order_id);
                $order->side = $payload->side;

                if ($order->save()) {
                    $client->sendKeyboard(
                        Keyboard::copies($payload->order_id),
                        "Копий ..."
                    );
                }

                break;
            case 'copy':
                $order       = Order::find($payload->order_id);
                $order->copy = $payload->copy;

                if ($order->save()) {
                    $buttons       = [];
                    $counter       = 1;
                    $desc          = '';

                    foreach (Terminal::all() as $terminal) {
                        $buttons[] = Button::create(
                            json_encode(
                                [
                                    'terminal_id' => $terminal->id,
                                    'order_id'    => $order->id,
                                    'command'     => 'terminal',
                                ]
                            ),
                            $counter,
                            'primary'
                        );
                        $desc      .= sprintf(
                            "%u - %s\n",
                            $counter,
                            $terminal->addr
                        );

                        $counter++;
                    }

                    $client->sendKeyboard(
                        Keyboard::create($buttons),
                        sprintf(
                            "Выберите терминал для распечатки:\n%s",
                            $desc
                        )
                    );
                }

                break;
            case 'terminal':
                $order              = Order::find($payload->order_id);
                $order->terminal_id = $payload->terminal_id;

                if ($order->save()) {
                    $client->send(
                        sprintf(
                            "Ссылка для оплаты\nЖми 👇\n" .
                            "%s",
                            url( "/invoice/" . $order->id)
                        )
                    );

                    $client->send(
                        "❗ВНИМАНИЕ❗\n" .
                        "Снятие средств с карты только после распечатки\n" .
                        "Если по тех.причинам распечатка не удалась\n" .
                        "автоматический возврат средств"
                    );

                    $client->sendKeyboard(
                        Keyboard::invoice(),
                        ""
                    );
                }

                break;
            case 'orders':
                $client->sendKeyboard(
                    Keyboard::orders($client),
                    "Подробнее о заказе ..."
                );

                break;
            case 'order':
                $order = Order::find($payload->order_id);

                if ($order === null) {
                    $client->send("Такого заказа нет");
                } else {
                    $client->send(
                        sprintf(
                            "Заказ %u\n" .
                            "Файл: %s\n" .
                            "Страниц: %u\n" .
                            "Копий: %u\n" .
                            "Терминал: %s\n" .
                            "Удалится через %s",
                            $order->id,
                            $order->filename,
                            $order->pages,
                            $order->copy,
                            $order->terminal->addr,
                            'некоторое время'
                        )
                    );

                    $client->sendKeyboard(
                        Keyboard::order($payload->order_id),
                        "Чтобы удалить заказ нажмите Cancel"
                    );
                }

                break;
            case 'cancel':
                $order = Order::find($payload->order_id);

                if ($order->utilize()) {
                    $client->sendKeyboard(
                        Keyboard::invoice(),
                        sprintf(
                            "Заказ %s удалён\n" .
                            "Деньги на Вашей карте разблокированы",
                            $order->filename
                        )
                    );
                }

                break;
            default:
                if (property_exists($this->object, "attachments")) {
                    $elementsNumber = count($this->object->attachments);

                    if ($elementsNumber == 1) {
                        $doc = $this->object->attachments[0];

                        if (property_exists($doc, "doc")) {
                            if ($doc->doc->ext == "pdf") {
                                if (count($client->orders) < Order::MAX_NUMBER) {
                                    $order = new Order();

                                    $order->create(
                                        $client->id,
                                        $doc->doc->title,
                                        $doc->doc->url,
                                        $doc->doc->size
                                    );
                                } else {
                                    $client->sendKeyboard(
                                        Keyboard::invoice(),
                                        "Максимально одновременно 4 заказа"
                                    );
                                }
                            } else {
                                $client->send("PDF мой любимый формат ❤");
                            }
                        } else {
                            $client->send("Мы же документ будем печатать? 📰");
                        }
                    } else {
                        $client->send("Я же просил только один PDF 😆");
                    }
                } else {
                    $client->send("Присылай один файл PDF 😉");
                }
        }
    }
}
