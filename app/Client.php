<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Client extends Model
{
    /**
     * Чтобы id не увеличивались при добавлении в БД
     * Берем id от vk
     *
     * @var bool
     */
	public $incrementing = false;

    /**
     * Паттерн для получения информации о клиенте VK
     */
	const USERS_GET = "https://api.vk.com/method/users.get?user_ids=%s&fields=domain&v=%s&access_token=%s";

    /**
     * Паттерн для отправки сообщения клиенту
     */
	const MESSAGES_SEND = "https://api.vk.com/method/messages.send?%s";

    /**
     * Отношение один ко многим между клиентами и заказами
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
	public function orders()
    {
		return $this->hasMany('App\Order');
	}

    /**
     * Регистрация клиента. Тянем доп инфу через VK API
     *
     * @param $userId
     *
     * @return bool
     */
	public function signUp($userId)
	{
        $about          = json_decode(
            file_get_contents(
                sprintf(
                    self::USERS_GET,
                    $userId,
                    config('vk.v'),
                    config('vk.token')
                )
            )
        );
        $response       = $about->response[0];
        $this->id       = $userId;
        $this->first    = $response->first_name;
        $this->last     = $response->last_name;
        $this->username = $response->domain;

		return $this->save();
	}

    /**
     * Посылает сообщение клиенту
     *
     * @param string $message
     * @param bool   $isKeyboard
     */
    public function send($message)
    {
        $requestParams                 = [];
        $requestParams['message']      = $message;
        $requestParams['user_id']      = $this->id;
        $requestParams['access_token'] = config("vk.token");
        $requestParams['v']            = config("vk.v");
        $httpParams                    = http_build_query($requestParams);

        file_get_contents(
            sprintf(
                self::MESSAGES_SEND,
                $httpParams
            )
        );
    }

    public function sendKeyboard(array $keyboard, $message)
    {
        $requestParams = [];
        $requestParams['message']      = $message;
        $requestParams['keyboard']     = json_encode($keyboard);
        $requestParams['user_id']      = $this->id;
        $requestParams['access_token'] = config("vk.token");
        $requestParams['v']            = config("vk.v");
        $httpParams                    = http_build_query($requestParams);

        $status = file_get_contents(
            sprintf(
                self::MESSAGES_SEND,
                $httpParams
            )
        );

        Log::info($status);
    }

    /**
     * Полное имя клиента
     *
     * @return string
     */
	public function fullName()
	{
		return $this->first . " " . $this->last;
	}

    /**
     * VK ссылка профиля
     *
     * @return string
     */
	public function profileUrl()
	{
		return "https://vk.com/id" . $this->id;
	}

}
