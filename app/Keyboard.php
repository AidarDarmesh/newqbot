<?php

namespace App;

use App\Button;
use App\Client;
use App\Order;

class Keyboard
{
    public static function create(array $buttons)
    {
        return [
            'one_time' => true,
            'buttons' => [$buttons], // в один ряд все кнопки
        ];
    }

    public static function sides($orderId)
    {
        $buttons = [];

        foreach ([1, 2] as $number) {
            $buttons[] = Button::create(
                json_encode(
                    [
                        'order_id' => $orderId,
                        'command'  => 'side',
                        'side'     => $number,
                    ]
                ),
                $number . '-sided',
                'primary'
            );
        }

        return self::create($buttons);
    }

    public static function copies($orderId)
    {
        $buttons = [];

        foreach ([1, 2, 3, 4] as $number) {
            $buttons[] = Button::create(
                json_encode(
                    [
                        'copy'     => $number,
                        'order_id' => $orderId,
                        'command'  => 'copy',
                    ]
                ),
                $number,
                'primary'
            );
        }

        return self::create($buttons);
    }

    public static function orders(Client $client)
    {
        $orderButtons = [];
        $orders       = $client->orders;

        if (count($orders) > 0) {
            foreach ($orders as $order) {
                $orderButtons[] = Button::create(
                    json_encode(
                        [
                            'order_id'    => $order->id,
                            'command'     => 'order',
                        ]
                    ),
                    substr($order->filename, 0, 37) . '...',
                    'primary'
                );
            }

            return self::create($orderButtons);
        } else {
            $client->send('У вас нет заказов');
        }
    }

    public static function order($orderId)
    {
        $buttonCancel = Button::createCancel($orderId);
        $buttonOrders = Button::createOrders();
        $buttonNew    = Button::createNew();

        return [
            'one_time' => true,
            'buttons' => [
                [$buttonCancel],
                [$buttonOrders, $buttonNew]
            ],
        ];
    }

    public static function invoice()
    {
        $buttonOrders = Button::createOrders();
        $buttonNew    = Button::createNew();

        return self::create([$buttonOrders, $buttonNew]);
    }
}
