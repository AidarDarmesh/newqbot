<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        *{
            font-family: Ubuntu, Calibri, sans-serif;
        }
        .card{
            display: inline-block;
            width: 24%;
        }
        .expire{
            display: inline-block;
            width: 30%;
        }
        .expire-group{
            display: inline-block;
            width: 74%;
        }
        .cvv-group{
            display: inline-block;
            width: 24%;
        }
        .btn-full{
            width: 100%;
        }
    </style>
</head>
<body>
    <form action="/paypost" method="POST">
    {{ csrf_field() }}
    <div class="container-fluid">
        <h3>Цена {{ $price }} тг</h3>
        <div class="form-group">
            <p><label for="card">Номер карты:</label></p>
            <input type="text" class="form-control card" name="card1" value="1234">
            <input type="text" class="form-control card" name="card2" value="1234">
            <input type="text" class="form-control card" name="card3" value="1234">
            <input type="text" class="form-control card" name="card4" value="1234">
        </div>
        <div class="form-group">
            <p><label for="name">Имя на карте:</label></p>
            <input type="text" class="form-control" name="name" value="Vasya Pupkin">
        </div>
        <div class="form-group expire-group">
            <p><label for="name">Срок действия:</label></p>
            <input type="text" class="form-control expire" name="month" value="07">
            <input type="text" class="form-control expire" name="year" value="22">
        </div>
        <div class="form-group cvv-group">
            <p><label for="name">CVV/CVC:</label></p>
            <input type="password" class="form-control" name="cvv" value="740">
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="">Сохранить для быстрого ввода</label>
        </div>
        <input type="hidden" name="invoice_id" value="{{ $invoice_id }}">
        <input type="submit" class="btn btn-primary btn-full" value="Оплатить заказ">
    </div>
    </form>
</body>
</html>
