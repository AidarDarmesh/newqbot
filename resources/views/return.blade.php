<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        *{
            font-family: Ubuntu, Calibri, sans-serif;
        }
        .btn-full{
            margin-top: 100px;
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <a href="{{ $link }}" class="btn btn-primary btn-full">Вернуться к боту</a>
    </div>
</body>
</html>
