<?php

Auth::routes();

// Бот
Route::post('/vk_bot', 'VkBotController@messageNew');

// Домашняя страница
Route::get('/home', 'HomeController@index');

Route::get('link', function(){
	return '{"type":"message_new","object":{"id":1193,"date":1520871169,"out":0,"user_id":207690737,"read_state":0,"title":"","body":"","attachments":[{"type":"doc","doc":{"id":446688146,"owner_id":207690737,"title":"PDF.pdf","size":4967272,"ext":"pdf","url":"https:\/\/vk.com\/doc207690737_446688146?hash=46c657e80431c00b41&dl=GIYDONRZGA3TGNY:1520871169:78bbda10d17923d21a&api=1&no_preview=1","date":1497029386,"type":1,"access_key":"df36717f54f744ef6d"}}]},"group_id":152165726,"secret":"sdf23rfsd98m9n"}';
});

Route::get('/after', 'InvoiceController@after');

// Клиенты
Route::get('/clients', 'ViewClientsController@index');
Route::get('/clients/addGet', 'ViewClientsController@addGet');
Route::post('/clients/addPost', 'ViewClientsController@addPost');
Route::get('/clients/{client}', 'ViewClientsController@details');
Route::get('/clients/{client}/editGet', 'ViewClientsController@editGet');
Route::post('/clients/{client}/editPost', 'ViewClientsController@editPost');
Route::get('/clients/{client}/delete', 'ViewClientsController@delete');

// Заказы
Route::get('/orders', 'ViewOrdersController@index');
Route::get('/orders/addGet', 'ViewOrdersController@addGet');
Route::post('/orders/addPost', 'ViewOrdersController@addPost');
Route::get('/orders/{order}', 'ViewOrdersController@details');
Route::get('/orders/{order}/editGet', 'ViewOrdersController@editGet');
Route::post('/orders/{order}/editPost', 'ViewOrdersController@editPost');
Route::get('/orders/{order}/delete', 'ViewOrdersController@delete');

// Инвойсы
Route::get('/invoices', 'ViewInvoicesController@index');
Route::get('/invoices/{invoice}', 'ViewInvoicesController@details');
Route::get('/invoices/{invoice}/delete', 'ViewInvoicesController@delete');

// Терминалы
Route::get('/terminals', 'ViewTerminalsController@index');
Route::get('/terminals/addGet', 'ViewTerminalsController@addGet');
Route::post('/terminals/addPost', 'ViewTerminalsController@addPost');
Route::get('/terminals/{terminal}', 'ViewTerminalsController@details');
Route::get('/terminals/{terminal}/editGet', 'ViewTerminalsController@editGet');
Route::post('/terminals/{terminal}/editPost', 'ViewTerminalsController@editPost');
Route::get('/terminals/{terminal}/delete', 'ViewTerminalsController@delete');

// Оплата
Route::get('/invoice/{order}', 'InvoiceController@pay');
Route::post('/paypost', 'InvoiceController@paypost');
Route::get('/backlink', 'InvoiceController@backlink');
Route::get('/fail_backlink', 'InvoiceController@fail_backlink');
Route::post('/postlink', 'InvoiceController@postlink');
Route::get('/fail_postlink', 'InvoiceController@fail_postlink');

// Выгрузка заказов
Route::get('/order/{order_id}/term/{term_id}', 'TerminalController@order');
Route::get('/code/order/{order_id}/term/{term_id}', 'TerminalController@code');
Route::get('/terminal/{terminalId}/printed/{order}', 'TerminalController@printed');
Route::get('/terminal/{terminal}/info', 'TerminalController@info');
